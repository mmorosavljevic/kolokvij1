﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kolokvij1 {
    sealed class Predmet {

        public Predmet(String nazivPredmeta, String odrediste, DateTime vrijemeIsporuke, double tezinaPredmeta, bool vatra,bool opasno) {
            NazivPredmeta = nazivPredmeta;
            Odrediste = odrediste;
            VrijemeIsporuke = vrijemeIsporuke;
            TezinaPredmeta = tezinaPredmeta;
            if (vatra && opasno) {
                throw new Exception("Opasan i zapaljiv predmet, oprez!");
            } else {
                this.vatra = vatra;
                Opasno = opasno;
            }
        }


        public override string ToString() {
            return String.Format("{0} / {1} / Zapaljivo - {2}",NazivPredmeta, Odrediste, Zapaljivo);
        }


        public String NazivPredmeta { get; set; }
        public String Odrediste { get; set; }
        public DateTime VrijemeIsporuke { get; set; }
        public double TezinaPredmeta { get; set; }
        public bool Opasno { get; set; }
        public String Zapaljivo { get {
                if(vatra) {
                    return "Da";
                } else {
                    return "Ne";
                }
            }
            set {
                if(vatra) {
                    throw new Exception("Zapaljiv predmet, oprez!");
                } else {
                    Zapaljivo = "Ne";
                }
            } }

        private bool vatra;
    }
}
