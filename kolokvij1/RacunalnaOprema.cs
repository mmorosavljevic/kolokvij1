﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kolokvij1 {
    class RacunalnaOprema : Skladiste,ISkladiste{
        public RacunalnaOprema(int maxPredmeta, bool oprema) : base(maxPredmeta, oprema) {
            if(maxPredmeta < max) {
                throw new Exception("Racunalna oprema mora pohraniti barem 500 predmeta");
            }
        }

        public override void DodajPredmet(String nazivPredmeta, String odrediste, DateTime vrijemeIsporuke, double tezinaPredmeta, bool vatra) {
            SviPredmeti.Add(new Predmet(nazivPredmeta, odrediste, vrijemeIsporuke, tezinaPredmeta, false,false));
        }

        public override void PrikaziPredmete() {
            foreach(var predmet in SviPredmeti) {
                if(DateTime.Now > predmet.VrijemeIsporuke) {
                    Console.WriteLine(predmet);
                } 
            }
        }

        private int max = 500;
    }
}
