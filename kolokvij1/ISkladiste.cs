﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kolokvij1 {
    interface ISkladiste {
        void DodajPredmet(String nazivPredmeta, String odrediste, DateTime vrijemeIsporuke, double tezinaPredmeta, bool vatra);
        void PrikaziPredmete();
    }
}
