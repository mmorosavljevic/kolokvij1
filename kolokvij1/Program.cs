﻿using System;

namespace kolokvij1 {
    class Program {
        static void Main(string[] args) {
            var datumIsporuke = new DateTime(2020, 5, 5, 8, 30, 52);
            Console.WriteLine(datumIsporuke);
            Predmet p = new Predmet("Paket1","Bjelovar", datumIsporuke,3.4,false,false);
            Console.WriteLine(p.ToString());

            RacunalnaOprema r = new RacunalnaOprema(520, false);
            ZapaljiveTvari z = new ZapaljiveTvari(100, true);

            r.PrikaziPredmete();
            r.DodajPredmet("Predmet1", "Bjelovar", new DateTime(2020, 4, 20, 12, 0, 0), 3.2, false);
            r.DodajPredmet("Predmet2", "Bjelovar", new DateTime(2020, 4, 20, 12, 0, 0), 3.2, false);
            r.DodajPredmet("Predmet3", "Bjelovar", new DateTime(2020, 4, 20, 12, 0, 0), 3.2, false);
            r.PrikaziPredmete();

            z.DodajPredmet("Predmet5", "Bjelovar", new DateTime(2020, 4, 20, 12, 0, 0), 3.2, true);
            z.PrikaziPredmete();

            Console.WriteLine("\n\n");
            r.PrikaziPredmete();

            Console.WriteLine("\n\n");
            z.PrikaziPredmete();

            



        }
    }
}
