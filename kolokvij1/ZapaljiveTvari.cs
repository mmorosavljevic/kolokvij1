﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kolokvij1 {
    class ZapaljiveTvari : Skladiste,ISkladiste{

        public ZapaljiveTvari(int maxPredmeta, bool oprema) :  base(maxPredmeta,oprema) {

        }


        public override void DodajPredmet(string nazivPredmeta, string odrediste, DateTime vrijemeIsporuke, double tezinaPredmeta, bool vatra) {
            if(vatra) {
                SviPredmeti.Add(new Predmet(nazivPredmeta, odrediste, vrijemeIsporuke, tezinaPredmeta, vatra,false));
            }
        }

        public override void PrikaziPredmete() {
            foreach(var predmet in SviPredmeti) {
                Console.WriteLine(predmet.ToString());
            }
        }
    }
}
