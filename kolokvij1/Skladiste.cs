﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kolokvij1 {
     abstract class Skladiste {
        
        public Skladiste(int max, bool oprema) {
            maxBroj = max;
            Oprema = oprema;
            SviPredmeti = new List<Predmet>();
        }

        public abstract void DodajPredmet(String nazivPredmeta, String odrediste, DateTime vrijemeIsporuke, double tezinaPredmeta, bool vatra);
        public virtual void PrikaziPredmete() {
            foreach(var predmet in SviPredmeti) {
                Console.WriteLine(predmet.ToString());
            }
        }

        public int MaxPredmeti { get {
                return maxBroj;
            }
            set {
                if(value > maxBroj) {
                    maxBroj = value;
                } else {
                    throw new Exception("Nije moguce postaviti manji maksimalan broj");
                }
            }
        }
        public bool Oprema { get; set; }

        protected List<Predmet> SviPredmeti;
        private int maxBroj;
    }
}
